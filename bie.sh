#!/usr/bin/env bash

## Dekatmantje: CIMIA
## Ki tan: 6256

######################################################
## Metamanyok-kalkilkamo-tala ka di ki              ##
## kamo fok itilize pou redi Raspbian, an metalafe  ##
## ki asou Raspberry Pi.                            ##
######################################################

###   KREY   ###
## Tit krey-la ki ke sevi pou lyanne kalkilkamo-a.
export krey_lyannaj="lyannaje"

###   MOUN   ###
## Mou ki pe lyanne kalkilkamo-a.
export moun_lyannaje=("pi")

###   KOMISYON   ###
## Konmisyon pou kore le moun-lan ki ka sevi
## ko-y di kalkilkamo-a bizwen plis dwa.
export komisyon_zwenmPlisDwa="Fok ou vin metalafe pou fe metmanyok-la pati."

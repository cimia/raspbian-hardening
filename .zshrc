# The following lines were added by compinstall

zstyle ':completion:*' auto-description ': %d'
zstyle ':completion:*' completer _complete _ignored
zstyle ':completion:*' expand prefix
zstyle ':completion:*' file-sort name
zstyle ':completion:*' format 'Mi sa man jwenn: %d.'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' ignore-parents parent
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' list-suffixes true
zstyle ':completion:*' menu select=1
zstyle ':completion:*' preserve-prefix '//[^/]##/'
zstyle ':completion:*' select-prompt I ni %l chans. // p: %p // B: %B // U: %U // S: %S // b: %b // u: %u // s: %s
zstyle ':completion:*' special-dirs true
zstyle ':completion:*' squeeze-slashes true
zstyle ':completion:*' verbose true
## Completion according to sudo possibilities.
zstyle ':completion::complete:*' gain-privileges 1
zstyle :compinstall filename '/home/cimia/.zshrc'

autoload -Uz compinit promptinit
compinit
promptinit
prompt clint
# End of lines added by compinstall
# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile.zsh
HISTSIZE=512
SAVEHIST=1024
setopt beep
bindkey -v
# End of lines configured by zsh-newuser-install

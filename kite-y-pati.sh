#!/bin/sh

## Metalafe: CIMIA
## Ki tan: 6256

###################################################################
## Metamanyok-kalkilkamo-tala ka redi metakalkilkamo Raspbian.   ##
###################################################################

larel=$(dirname "$0")
source $larel/bie.sh

## Gade si tout bagay ka mache.
if [ $(id -u) -ne 0 ]
then
   echo "$komisyon_zwenmPlisDwa"

   exit 1

fi

###   GRAPKAMO   ###
cp etc/ssh/sshd_config /etc/ssh/sshd_config
cp etc/network/interfaces /etc/network/interfaces

###   DWA   ###
## Krey
groupadd $krey_lyannaj
## Moun
for moun in $moun_lyannaje
do
   usermod -aG $krey_lyannaj $moun

done
## GRAPKAMO
chown :$krey_lyannaj /etc/network/interfaces

reboot now

